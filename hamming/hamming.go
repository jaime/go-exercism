package hamming

import (
	"fmt"
)

// Distance calculates the Hamming distance between two strings of equal length.
// It returns an error if there is a length mismatch between both strings.
func Distance(a, b string) (int, error) {
	if len(a) != len(b) {
		return 0, fmt.Errorf("string a length: %d mismatch string b length: %d", len(a), len(b))
	}

	diff := 0

	for i, c := range a {
		if string(c) != string(b[i]) {
			diff++
		}
	}

	return diff, nil
}
