package raindrops

import "strconv"

// Convert i into a factored string.
// Factor of 3 adds the word Pling.
// Factor of 5 adds the word Plang.
// Factor of 7 adds the word Plong.
// Otherwise the number is returned as a string using the strconv package.
func Convert(i int) string {
	sb := ""
	if i%3 == 0 {
		sb += "Pling"
	}

	if i%5 == 0 {
		sb += "Plang"
	}

	if i%7 == 0 {
		sb += "Plong"
	}

	if sb == "" {
		sb = strconv.Itoa(i)
	}

	return sb
}
