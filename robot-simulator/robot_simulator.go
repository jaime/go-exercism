package robot

// step 1

const (
	N Dir = iota
	S
	E
	W
)

func (d Dir) String() string {
	//valid directions N, S, E, W
	switch d {
	case 0:
		return "N"
	case 1:
		return "S"
	case 2:
		return "E"
	case 3:
		return "W"
	default:
		return ""
	}
}

func Right() {
	switch Step1Robot.Dir {
	case N:
		Step1Robot.Dir = E
	case E:
		Step1Robot.Dir = S
	case S:
		Step1Robot.Dir = W
	case W:
		Step1Robot.Dir = N
	default:
		// noop
	}
}
func Left() {
	switch Step1Robot.Dir {
	case N:
		Step1Robot.Dir = W
	case E:
		Step1Robot.Dir = N
	case S:
		Step1Robot.Dir = E
	case W:
		Step1Robot.Dir = S
	default:
		// noop
	}
}
func Advance() {
	switch Step1Robot.Dir {
	case N:
		Step1Robot.Y++
	case E:
		Step1Robot.X++
	case S:
		Step1Robot.Y--
	case W:
		Step1Robot.X--
	default:
		// noop
	}
}
